package ru.manufacture.types.format;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class EnumFormat extends Format<Enum> {
    private Class<? extends Enum> type;

    public EnumFormat(Class<? extends Enum> type) {
        this.type = type;
    }

    @Override
    public String format(Enum value) {
        return null == value ? null : value.name();
    }

    @Override
    public Enum parse(String s) {
        return isBlank(s) ? null : Enum.valueOf(type, s);
    }
}
