package ru.manufacture.types.format;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class ShortFormat extends Format<Short> {
    @Override
    public String format(Short value) {
        return null == value ? null : value.toString();
    }

    @Override
    public Short parse(String s) {
        return isBlank(s) ? null : Short.valueOf(s);
    }
}
