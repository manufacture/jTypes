package ru.manufacture.types;

/**
 * @author Degtyarev Roman
 * @date 23.07.2015.
 */
public class UnsupportedTypeException extends IllegalArgumentException {
    public UnsupportedTypeException(String s) {
        super(s);
    }

    public UnsupportedTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
