package ru.manufacture.types.format;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class FloatFormat extends Format<Float> {
    @Override
    public String format(Float value) {
        return null == value ? null : value.toString();
    }

    @Override
    public Float parse(String s) {
        return isBlank(s) ? null : Float.valueOf(s);
    }
}
