package ru.manufacture.types.format;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class IntegerFormat extends Format<Integer> {
    @Override
    public String format(Integer value) {
        return null == value ? null : value.toString();
    }

    @Override
    public Integer parse(String s) {
        return isBlank(s) ? null : Integer.valueOf(s);
    }
}
