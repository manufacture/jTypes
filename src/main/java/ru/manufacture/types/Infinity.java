package ru.manufacture.types;

/**
 * @author Degtyarev Roman
 * @date 28.08.2015.
 */
public enum Infinity {
    NEGATIVE {
        @Override
        public String toString() {
            return "-oo";
        }
    },
    POSITIVE {
        @Override
        public String toString() {
            return "+oo";
        }
    };
    public abstract String toString();
}
