package ru.manufacture.types.format;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class BooleanFormat extends Format<Boolean> {
    @Override
    public String format(Boolean value) {
        return null == value ? null : value.toString();
    }

    @Override
    public Boolean parse(String s) {
        if (isBlank(s)) {
            return null;
        }
        if ("true".equalsIgnoreCase(s.trim()))
            return Boolean.TRUE;
        if ("false".equalsIgnoreCase(s.trim()))
            return Boolean.FALSE;
        throw new IllegalArgumentException("Invalid boolean value: " + s);
    }
}
