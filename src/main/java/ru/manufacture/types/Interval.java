/**
 * @author Degtyarev Roman
 * @date 28.08.2015.
 */
package ru.manufacture.types;

import java.io.Serializable;

/**
 * <h4>Интервал</h4>
 * <p>
 *     Утилитный класс для упрощения работы с интервалами значений
 *
 * @param <DataType> тип данных интервала
 */
public class Interval<DataType extends Comparable<DataType>> implements Serializable {
    /**
     * создает интервал от указанного значения включительно до плюс бесконечности
     * @param fromValue начало интервала
     * @param <DataType> тип данных интервала
     * @return [X;+oo) интервал от указанного значения до плюс бесконечности
     */
    public static <DataType extends Comparable<DataType>> Interval<DataType> from(DataType fromValue) {
        return from(fromValue, Border.INCLUDE);
    }

    /**
     * создает интервал от указанного значения до плюс бесконечности
     * @param fromValue начало интервала
     * @param fromBorder граница начала интервала
     * @param <DataType> тип данных интервала
     * @return {X;+oo) интервал от указанного значения до плюс бесконечности
     */
    public static <DataType extends Comparable<DataType>> Interval<DataType> from(DataType fromValue, Border fromBorder) {
        return new Interval<>(fromValue, null, fromBorder, null);
    }

    /**
     * создает интервал от минус бесконечности до указанного значения включительно
     * @param toValue конец интервала
     * @param <DataType> тип данных интервала
     * @return &#040;-oo;X] интервал от минус бесконечности до указанного значения включительно
     */
    public static <DataType extends Comparable<DataType>> Interval<DataType> to(DataType toValue) {
        return to(toValue, Border.INCLUDE);
    }

    /**
     * создает интервал от минус бесконечности до указанного значения
     * @param toValue конец интервала
     * @param toBorder граница конца интервала
     * @param <DataType> тип данных интервала
     * @return &#040;-oo;X} интервал от минус бесконечности до указанного значения
     */
    public static <DataType extends Comparable<DataType>> Interval<DataType> to(DataType toValue, Border toBorder) {
        return new Interval<>(null, toValue, null, toBorder);
    }

    /**
     * создает интервал от минус бесконечности до плюс бесконечности
     * @return интервал (-oo;+oo)
     */
    public static Interval infinity() {
        return new Interval(null, null, null, null);
    }

    /**
     * Граница интервала
     */
    public enum Border {
        /**
         * Включающая граница интервала
         */
        INCLUDE {
            @Override
            public String left() {
                return "[";
            }

            @Override
            public String right() {
                return "]";
            }
        },
        /**
         * Исключающая граница интервала
         */
        EXCLUDE {
            @Override
            public String left() {
                return "(";
            }

            @Override
            public String right() {
                return ")";
            }
        };
        public abstract String left();
        public abstract String right();
    }
    /**
     * начало интервала
     */
    private DataType fromValue;
    /**
     * конец интервала
     */
    private DataType toValue;
    /**
     * граница начала интервала
     */
    private Border fromBorder;
    /**
     * граница конца интервала
     */
    private Border toBorder;

    /**
     * конструктор
     * @param fromValue начало интервала
     * @param toValue конец интервала
     * @param fromBorder граница начала интервала
     * @param toBorder граница конца интервала
     */
    public Interval(DataType fromValue, DataType toValue, Border fromBorder, Border toBorder) {
        this.fromValue = fromValue;
        this.toValue = toValue;
        this.fromBorder = isInfinityFromValue() ? Border.EXCLUDE : fromBorder;
        this.toBorder = isInfinityToValue() ? Border.EXCLUDE : toBorder;
        if (null == this.fromBorder) {
            this.fromBorder = Border.INCLUDE;
        }
        if (null == this.toBorder) {
            this.toBorder = Border.INCLUDE;
        }
        if (!isInfinityFromValue() && !isInfinityToValue()) {
            int compare = toValue.compareTo(fromValue);
            if (compare < 0) {
                throw new IllegalArgumentException("Illegal interval, invalid border values order: " + this);
            }
        }
    }

    /**
     * конструктор интервала включающего границы
     * @param fromValue начало интервала
     * @param toValue конец интервала
     */
    public Interval(DataType fromValue, DataType toValue) {
        this(fromValue, toValue, Border.INCLUDE, Border.INCLUDE);
    }

    /**
     * @return возвращает начало интервала
     */
    public DataType getFromValue() {
        return fromValue;
    }

    /**
     * @return возвращает конец интервала
     */
    public DataType getToValue() {
        return toValue;
    }

    /**
     * @return возвращает границу начала интервала
     */
    public Border getFromBorder() {
        return fromBorder;
    }

    /**
     * @return возвращает границу конца интервала
     */
    public Border getToBorder() {
        return toBorder;
    }

    /**
     * @return true - если это интервал от минус бесконечности
     */
    public boolean isInfinityFromValue() {
        return null == fromValue;
    }

    /**
     * @return true - если это интервал до плюс бесконечности
     */
    public boolean isInfinityToValue() {
        return null == toValue;
    }

    /**
     * проверяет входит ли значение в интервал
     * @param value значение для проверки
     * @return true - если значение входит в интервал
     */
    public boolean match(DataType value) {
        return null != value && matchFrom(value) && matchTo(value);
    }

    /**
     * проверяет включает ли в себя данный интервал другой интервал
     * @param other интервал
     * @return true - если обе границы интервала входят в данный интервал
     */
    public boolean includes(Interval<DataType> other) {
        boolean matchFrom = isInfinityFromValue() || match(other.fromValue);
        boolean matchTo = isInfinityToValue() || match(other.toValue);
        return matchFrom && matchTo;
    }

    /**
     * проверяет пересекаются ли интервалы, т.е. одна из границ указанного интервала входит в данный интервал
     * @param other интервал
     * @return true - если интервалы пересекаются
     */
    public boolean intersects(Interval<DataType> other) {
        return match(other.fromValue) || match(other.toValue);
    }

    /**
     * Возвращает объединение интервалов
     * @param other интервал, с которым выполняется объединение
     * @return объединенный интервал
     */
    public Interval<DataType> join(Interval<DataType> other) {
        if (!intersects(other)) {
            throw new IllegalArgumentException("Interval hasn't intersection with " + other);
        }
        DataType fromValue;
        DataType toValue;
        Border fromBorder;
        Border toBorder;
        if (!other.isInfinityFromValue() && matchFrom(other.fromValue)) {
            fromValue = this.fromValue;
            fromBorder = this.fromBorder;
        } else {
            fromValue = other.fromValue;
            fromBorder = other.fromBorder;
        }
        if (!other.isInfinityToValue() && matchTo(other.toValue)) {
            toValue = this.toValue;
            toBorder = this.toBorder;
        } else {
            toValue = other.toValue;
            toBorder = other.toBorder;
        }
        return new Interval<>(fromValue, toValue, fromBorder, toBorder);
    }

    /**
     * Возвращает пересечение интервалов
     * @param other интервал, с которым выполняется пересечение
     * @return интервал-пересечение
     */
    public Interval<DataType> intersect(Interval<DataType> other) {
        if (!intersects(other)) {
            throw new IllegalArgumentException("Interval hasn't intersection with " + other);
        }
        DataType fromValue;
        DataType toValue;
        Border fromBorder;
        Border toBorder;
        if (other.isInfinityFromValue() || !matchFrom(other.fromValue)) {
            fromValue = this.fromValue;
            fromBorder = this.fromBorder;
        } else {
            fromValue = other.fromValue;
            fromBorder = other.fromBorder;
        }
        if (other.isInfinityToValue() || !matchTo(other.toValue)) {
            toValue = this.toValue;
            toBorder = this.toBorder;
        } else {
            toValue = other.toValue;
            toBorder = other.toBorder;
        }
        return new Interval<>(fromValue, toValue, fromBorder, toBorder);
    }

    /**
     * Возвращает интервал, начало которого устанавливается в значение переданного параметра
     * @param fromValue значение начала интервала
     * @return интервал
     */
    public Interval<DataType> setFromValue(DataType fromValue) {
        return new Interval<>(fromValue, this.toValue, this.fromBorder, this.toBorder);
    }

    /**
     * Возвращает интервал, конец которого устанавливается в значение переданного параметра
     * @param toValue значение конца интервала
     * @return интервал
     */
    public Interval<DataType> setToValue(DataType toValue) {
        return new Interval<>(this.fromValue, toValue, this.fromBorder, this.toBorder);
    }

    /**
     * Возвращает интервал, начальная граница которого устанавливается в значение переданного параметра
     * @param fromBorder начальная граница интервала
     * @return интервал
     */
    public Interval<DataType> setFromBorder(Border fromBorder) {
        return new Interval<>(this.fromValue, this.toValue, fromBorder, this.toBorder);
    }

    /**
     * Возвращает интервал, конечная граница которого устанавливается в значение переданного параметра
     * @param toBorder конечная граница интервала
     * @return интервал
     */
    public Interval<DataType> setToBorder(Border toBorder) {
        return new Interval<>(this.fromValue, this.toValue, this.fromBorder, toBorder);
    }

    /**
     * проверяет соотетствует ли значение ограничению конца интервала
     * @param value значение для проверки
     * @return true - если значение соотетствует ограничению конца интервала
     */
    private boolean matchTo(DataType value) {
        if (isInfinityToValue()) {
            return true;
        }
        int compare = toValue.compareTo(value);
        return toBorder == Border.INCLUDE ? compare >= 0 : compare > 0;
    }

    /**
     * проверяет соотетствует ли значение ограничению начала интервала
     * @param value значение для проверки
     * @return true - если значение соотетствует ограничению начала интервала
     */
    private boolean matchFrom(DataType value) {
        if (isInfinityFromValue()) {
            return true;
        }
        int compare = fromValue.compareTo(value);
        return fromBorder == Border.INCLUDE ? compare <= 0 : compare < 0;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(fromBorder.left());
        if (isInfinityFromValue()) {
            buffer.append(Infinity.NEGATIVE);
        } else {
            Type type = Type.of(fromValue.getClass());
            buffer.append(type.toString(fromValue));
        }
        buffer.append(";");
        if (isInfinityToValue()) {
            buffer.append(Infinity.POSITIVE);
        } else {
            Type type = Type.of(toValue.getClass());
            buffer.append(type.toString(toValue));
        }
        buffer.append(toBorder.right());
        return buffer.toString();
    }
}
