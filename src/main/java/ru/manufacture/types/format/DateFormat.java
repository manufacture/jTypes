package ru.manufacture.types.format;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 23.07.2015.
 */
public class DateFormat extends Format<Date> {
    public static final String [] PATTERN = {
            "dd.MM.yyyy HH:mm:ss",
            "dd/MM/yyyy HH:mm:ss",
            "dd-MM-yyyy HH:mm:ss",
            "dd.MM.yyyy",
            "dd/MM/yyyy",
            "dd-MM-yyyy"
    };

    public static final String DEFAULT_PATTERN = PATTERN[0];

    @Override
    public String format(Date value) {
        if (null == value) {
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(value);
        int time = calendar.get(Calendar.HOUR) + calendar.get(Calendar.MINUTE) + calendar.get(Calendar.SECOND);
        if (time > 0)
            return new SimpleDateFormat(DEFAULT_PATTERN).format(value);
        else
            return new SimpleDateFormat(PATTERN[3]).format(value);
    }

    @Override
    public Date parse(String s) {
        if (isBlank(s)) {
            return null;
        }
        for (int i = 0; i < PATTERN.length; i++) {
            try {
                return new SimpleDateFormat(PATTERN[i]).parse(s);
            } catch (ParseException ignore) {
            }
        }
        throw new IllegalArgumentException("Invalid date format: " + s);
    }
}
