package ru.manufacture.types.format;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class LongFormat extends Format<Long> {
    @Override
    public String format(Long value) {
        return null == value ? null : value.toString();
    }

    @Override
    public Long parse(String s) {
        return isBlank(s) ? null : Long.valueOf(s);
    }
}
