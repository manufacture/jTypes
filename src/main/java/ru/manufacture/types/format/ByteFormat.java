package ru.manufacture.types.format;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class ByteFormat extends Format<Byte> {
    @Override
    public String format(Byte value) {
        return null == value ? null : value.toString();
    }

    @Override
    public Byte parse(String s) {
        return isBlank(s) ? null : Byte.valueOf(s);
    }
}
