package ru.manufacture.types.format;

import java.math.BigDecimal;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class BigDecimalFormat extends Format<BigDecimal> {
    @Override
    public String format(BigDecimal value) {
        return null == value ? null : value.toString();
    }

    @Override
    public BigDecimal parse(String s) {
        return isBlank(s) ? null : new BigDecimal(s);
    }
}
