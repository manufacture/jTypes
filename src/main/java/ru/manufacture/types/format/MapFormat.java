package ru.manufacture.types.format;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 25.08.2015.
 */
public class MapFormat extends Format<Map<String, String>> {
    @Override
    public String format(Map<String, String> value) {
        if (null == value || value.isEmpty()) {
            return null;
        }
        Properties properties = new Properties();
        properties.putAll(value);
        StringWriter writer = new StringWriter();
        try {
            properties.store(writer, "");
        } catch (IOException e) {
            throw new IllegalArgumentException("Fails to format map: " + value, e);
        }
        return writer.getBuffer().toString();
    }

    @Override
    public Map<String, String> parse(String s) {
        if (isBlank(s)) {
            return null;
        }
        Properties properties = new Properties();
        try {
            properties.load(new StringReader(s));
        } catch (IOException e) {
            throw new IllegalArgumentException("Fails to parse map: " + s, e);
        }
        return new ConcurrentHashMap<>((Map)properties);
    }
}
