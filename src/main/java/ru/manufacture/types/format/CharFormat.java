package ru.manufacture.types.format;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class CharFormat extends Format<Character> {
    @Override
    public String format(Character value) {
        return null == value ? null : value.toString();
    }

    @Override
    public Character parse(String s) {
        return isBlank(s) ? null : Character.valueOf(s.charAt(0));
    }
}
