package ru.manufacture.types.format;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 26.08.2015.
 */
public class XmlFormat extends Format<Document> {
    @Override
    public String format(Document xml) {
        if (null == xml) {
            return null;
        }
        StringWriter out = new StringWriter();
        OutputFormat format = OutputFormat.createPrettyPrint();
        XMLWriter writer = new XMLWriter(out, format);
        try {
            writer.write(xml);
        } catch (IOException e) {
            throw new IllegalStateException("Fails to format xml document", e);
        }
        return out.getBuffer().toString();
    }

    @Override
    public Document parse(String xml) {
        if (isBlank(xml)) {
            return null;
        }
        SAXReader reader = new SAXReader();
        try {
            return reader.read(new StringReader(xml));
        } catch (DocumentException e) {
            throw new IllegalArgumentException("Fails to parse xml document: " + xml, e);
        }
    }
}
