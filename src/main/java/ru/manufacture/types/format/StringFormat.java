package ru.manufacture.types.format;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class StringFormat extends Format<String> {
    @Override
    public String format(String value) {
        return value;
    }

    @Override
    public String parse(String s) {
        return s;
    }
}
