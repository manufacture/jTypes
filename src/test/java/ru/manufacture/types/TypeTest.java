package ru.manufacture.types;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import org.junit.Assert;
import org.junit.Test;

public class TypeTest extends Assert {

    @Test
    public void testDetect() throws Exception {
        assertTrue(Type.detect("23.07.2015") == Type.Date);
        assertTrue(Type.detect("true") == Type.Boolean);
        assertTrue(Type.detect("100.0") == Type.BigDecimal);
        assertTrue(Type.detect("100") == Type.Long);
        assertTrue(Type.detect("Hallo") == Type.String);
    }

    @Test
    public void testCast() throws Exception {
        assertTrue(new BigDecimal("100").equals(Type.BigDecimal.cast("100")));
        assertTrue(new Long(100).equals(Type.Long.cast("100")));
        assertTrue(new Integer(100).equals(Type.Integer.cast("100")));
        assertTrue(Boolean.TRUE.equals(Type.Boolean.cast("true")));
        assertTrue(new SimpleDateFormat("dd.MM.yyyy").parse("23.07.2015").equals(Type.Date.cast("23.07.2015")));
        assertTrue("100".equals(Type.String.cast("100")));
    }
}