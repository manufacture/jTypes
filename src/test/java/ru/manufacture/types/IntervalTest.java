package ru.manufacture.types;

import java.util.Date;
import org.junit.Assert;
import org.junit.Test;
import ru.manufacture.types.format.Format;

public class IntervalTest extends Assert {
    @Test
    public void execute() {
        Interval<Long> interval = new Interval<>(null, 100L, Interval.Border.EXCLUDE, Interval.Border.INCLUDE);
        System.out.println(interval);
        //System.out.println(new Interval<>(10, 0, Interval.Border.INCLUDE, Interval.Border.INCLUDE));
        System.out.println(interval.match(10L));
        System.out.println(interval.match(100L));
        System.out.println(interval.match(101L));

        System.out.println(Interval.from(100));
        System.out.println(Interval.to(100));
    }

    @Test
    public void testComprise() {
        Date archiveDate = Format.toObject(Date.class, "01.01.2012");
        Interval<Date> archiveInterval = Interval.to(archiveDate);
        System.out.println(archiveInterval);

        Interval<Date> currentMonth = new Interval<>(
                Format.toObject(Date.class, "01.10.2015"),
                Format.toObject(Date.class, "27.10.2015"),
                Interval.Border.INCLUDE,
                Interval.Border.INCLUDE
        );

        Interval<Date> currentWeek = new Interval<>(
                Format.toObject(Date.class, "26.10.2015"),
                Format.toObject(Date.class, "30.10.2015"),
                Interval.Border.INCLUDE,
                Interval.Border.INCLUDE
        );

        Interval<Date> currentDay = new Interval<>(
                Format.toObject(Date.class, "27.10.2015"),
                Format.toObject(Date.class, "27.10.2015"),
                Interval.Border.INCLUDE,
                Interval.Border.INCLUDE
        );

        assertTrue(!archiveInterval.includes(currentMonth));
        assertTrue(!archiveInterval.includes(currentWeek));
        assertTrue(!archiveInterval.includes(currentDay));

        Interval<Date> searchInterval = new Interval<>(
                Format.toObject(Date.class, "27.10.2011"),
                Format.toObject(Date.class, "27.11.2011"),
                Interval.Border.INCLUDE,
                Interval.Border.INCLUDE
        );
        assertTrue(archiveInterval.includes(searchInterval));
    }

    @Test
    public void testJoinIntersect() {
        Interval<Date> currentMonth = new Interval<>(
                Format.toObject(Date.class, "01.10.2015"),
                Format.toObject(Date.class, "27.10.2015"),
                Interval.Border.INCLUDE,
                Interval.Border.INCLUDE
        );

        Interval<Date> nextMonth = new Interval<>(
                Format.toObject(Date.class, "20.10.2015"),
                Format.toObject(Date.class, "27.11.2015"),
                Interval.Border.INCLUDE,
                Interval.Border.INCLUDE
        );

        Interval<Date> fromToday = Interval.from(Format.toObject(Date.class, "27.10.2015"));

        System.out.println(currentMonth.join(nextMonth));
        System.out.println(currentMonth.intersect(nextMonth));
        System.out.println(currentMonth.intersect(fromToday));
        System.out.println(currentMonth.join(fromToday));
    }
}