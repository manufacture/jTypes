package ru.manufacture.types.format;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang.StringUtils;

/**
 * @author Degtyarev Roman
 * @date 26.08.2015.
 */
public class SetFormat extends Format<Set<String>> {

    public static final char SEPARATOR = ',';

    @Override
    public String format(Set<String> value) {
        return StringUtils.join(value, SEPARATOR);
    }

    @Override
    public Set<String> parse(String s) {
        String[] strings = StringUtils.split(s, SEPARATOR);
        return null == strings ? null : new HashSet<String>(Arrays.asList(strings));
    }
}
