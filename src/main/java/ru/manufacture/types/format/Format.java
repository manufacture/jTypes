package ru.manufacture.types.format;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.dom4j.Document;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public abstract class Format<Type> {
    private static Map<Class, Format> formats = init();

    private static Map<Class, Format> init() {
        Map<Class, Format> formats = new ConcurrentHashMap<Class, Format>();

        BooleanFormat booleanFormat = new BooleanFormat();
        ByteFormat byteFormat = new ByteFormat();
        ShortFormat shortFormat = new ShortFormat();
        IntegerFormat integerFormat = new IntegerFormat();
        LongFormat longFormat = new LongFormat();
        DoubleFormat doubleFormat = new DoubleFormat();
        FloatFormat floatFormat = new FloatFormat();
        CharFormat charFormat = new CharFormat();

        formats.put(Boolean.class, booleanFormat);
        formats.put(Byte.class, byteFormat);
        formats.put(Short.class, shortFormat);
        formats.put(Integer.class, integerFormat);
        formats.put(Long.class, longFormat);
        formats.put(Double.class, doubleFormat);
        formats.put(BigDecimal.class, new BigDecimalFormat());
        formats.put(Date.class, new DateFormat());
        formats.put(String.class, new StringFormat());
        formats.put(Float.class, floatFormat);
        formats.put(Character.class, charFormat);
        formats.put(boolean.class, booleanFormat);
        formats.put(byte.class, byteFormat);
        formats.put(short.class, shortFormat);
        formats.put(int.class, integerFormat);
        formats.put(long.class, longFormat);
        formats.put(double.class, doubleFormat);
        formats.put(float.class, floatFormat);
        formats.put(char.class, charFormat);
        formats.put(Map.class, new MapFormat());
        formats.put(Set.class, new SetFormat());
        formats.put(Document.class, new XmlFormat());
        return formats;
    }

    public static void put(Class type, Format format) {
        formats.put(type, format);
    }

    public static boolean contains(Class type) {
        return formats.containsKey(type);
    }

    public static Format instance(Class type) {
        Format format = formats.get(type);
        if (null == format) {
            throw new IllegalArgumentException("Format type not supported: " + type);
        }
        return format;
    }

    public static <Type> Type toObject(Class<? extends Type> type, String s) {
        return isBlank(s) ? null : (Type) instance(type).parse(s);
    }

    public static String toString(Object value) {
        return null == value ? null : instance(value.getClass()).format(value);
    }

    public abstract String format(Type value);
    public abstract Type parse(String s);
}
