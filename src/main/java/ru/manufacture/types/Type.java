package ru.manufacture.types;

import java.math.BigDecimal;
import java.util.Date;
import ru.manufacture.types.format.Format;


import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 17.06.2015.
 */
public enum Type {
    Long {
        @Override
        public String toString(Object value) {
            return Format.instance(Long.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return Long.class;
        }

        @Override
        public Long cast(String value) {
            return (java.lang.Long) Format.instance(Long.class).parse(value);
        }
    }, Integer {
        @Override
        public String toString(Object value) {
            return Format.instance(Integer.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return Integer.class;
        }

        @Override
        public Integer cast(String value) {
            return (java.lang.Integer) Format.instance(java.lang.Integer.class).parse(value);
        }
    }, Date {
        @Override
        public String toString(Object value) {
            return Format.instance(Date.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return Date.class;
        }

        @Override
        public Date cast(String value) {
            return (java.util.Date) Format.instance(java.util.Date.class).parse(value);
        }
    }, BigDecimal {
        @Override
        public String toString(Object value) {
            return Format.instance(BigDecimal.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return BigDecimal.class;
        }

        @Override
        public BigDecimal cast(String value) {
            return (java.math.BigDecimal) Format.instance(java.math.BigDecimal.class).parse(value);
        }
    }, Boolean {
        @Override
        public String toString(Object value) {
            return Format.instance(Boolean.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return Boolean.class;
        }

        @Override
        public Boolean cast(String value) {
            return (java.lang.Boolean) Format.instance(java.lang.Boolean.class).parse(value);
        }
    }, String {
        @Override
        public String toString(Object value) {
            return Format.instance(String.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return String.class;
        }

        @Override
        public String cast(String value) {
            return value;
        }
    }, Byte {
        @Override
        public String toString(Object value) {
            return Format.instance(Byte.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return Byte.class;
        }

        @Override
        public Byte cast(String value) {
            return (java.lang.Byte) Format.instance(java.lang.Byte.class).parse(value);
        }
    }, Short {
        @Override
        public String toString(Object value) {
            return Format.instance(Short.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return Short.class;
        }

        @Override
        public Short cast(String value) {
            return (java.lang.Short) Format.instance(java.lang.Short.class).parse(value);
        }
    }, Char {
        @Override
        public String toString(Object value) {
            return Format.instance(Character.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return Character.class;
        }

        @Override
        public Character cast(String value) {
            return (Character) Format.instance(Character.class).parse(value);
        }
    }, Float {
        @Override
        public String toString(Object value) {
            return Format.instance(Float.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return Float.class;
        }

        @Override
        public Float cast(String value) {
            return (java.lang.Float) Format.instance(java.lang.Float.class).parse(value);
        }
    }, Double {
        @Override
        public String toString(Object value) {
            return Format.instance(Double.class).format(value);
        }

        @Override
        public Class getJavaType() {
            return Double.class;
        }

        @Override
        public Double cast(String value) {
            return (java.lang.Double) Format.instance(java.lang.Double.class).parse(value);
        }
    };

    public static Type of(String typeName) {
        for (Type type : values()) {
            if (type.name().equalsIgnoreCase(typeName))
                return type;
        }
        throw new IllegalArgumentException("Type not supported: " + typeName);
    }

    public static Type detect(String value) {
        if (isBlank(value)) {
            throw new IllegalArgumentException("Can't detect type of null value");
        }
        for (Type type : new Type[]{Date, Boolean, Long, BigDecimal, String}) {
            try {
                Object object = type.cast(value);
                return type;
            } catch (Exception ignore) {}
        }
        throw new UnsupportedTypeException("Type of value not detected: " + value);
    }

    public static Type of(Class typeClass) {
        for (Type type : values()) {
            if (type.getJavaType().equals(typeClass))
                return type;
        }
        throw new IllegalArgumentException("Type not supported: " + typeClass);
    }

    public abstract <T> T cast(String value);
    public abstract String toString(Object value);
    public abstract Class getJavaType();
}
