package ru.manufacture.types.format;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * @author Degtyarev Roman
 * @date 22.07.2015.
 */
public class DoubleFormat extends Format<Double> {
    @Override
    public String format(Double value) {
        return null == value ? null : value.toString();
    }

    @Override
    public Double parse(String s) {
        return isBlank(s) ? null : Double.valueOf(s);
    }
}
